//
//  RescuerTabView.swift
//  ImpawsibbleSwiftUI
//
//  Created by Michael Randicha Gunawan Santoso on 31/07/20.
//  Copyright © 2020 Najibullah Ulul Albab. All rights reserved.
//

import SwiftUI

struct RescuerTabView: View {
    @EnvironmentObject var settings: UserSettings
    
    var body: some View {
        TabView {
            NavigationView {
                Text("Pet Sanctuary")
                .navigationBarItems(trailing:
                    Button(action: {
                        self.settings.reset()
                    }) {
                    Text("Reset")
                    }
                )
            }
            .tabItem {
                Image(systemName: "house.fill")
                Text("Pet Sanctuary")
            }
            
            NavigationView {
                Text("Rescuer Profile")
            }
            .tabItem {
                Image(systemName: "person.fill")
                Text("Profile")
            }
        }
        .accentColor(.orange)
    }
}

struct RescuerTabView_Previews: PreviewProvider {
    static var previews: some View {
        RescuerTabView()
    }
}
