//
//  RescuerAgreement.swift
//  ImpawsibbleSwiftUI
//
//  Created by Najibullah Ulul Albab on 05/08/20.
//  Copyright © 2020 Najibullah Ulul Albab. All rights reserved.
//

import SwiftUI

struct RescuerAgreement: View {
    @State var agreements: [String] = ["","",""]
    @State var description: String = ""
    var body: some View {
        ScrollView{
            ZStack{
                Rectangle()
                    .fill(Color("Secondary"))
                    .cornerRadius(20)
                    
                VStack(alignment: .leading){
                    Text("Hello!")
                    .font(.system(.largeTitle, design: .rounded))
                    .bold()
                    .foregroundColor(Color("Primary Text"))
                    Text("Untuk mengenal kamu lebih jauh kami memerlukan beberapa informasi mengenai shelter yang akan kamu daftarkan dalam aplikasi kami, serta mengetahui agreement seperti apa yang shelter kalian perlukan untuk para calon adopter")
                    .font(.system(.body, design: .rounded))
                    .bold()
                    .foregroundColor(Color("Primary Text"))
                }.padding()
            }
            ZStack(alignment: .topLeading){
                Rectangle()
                .fill(Color("Secondary"))
                .cornerRadius(20)
                VStack(alignment: .leading){
                    ForEach(0..<self.agreements.count){ index in
                        Text("Perjanjian \(index+1)")
                        .font(.system(.body, design: .rounded))
                        .bold()
                        .foregroundColor(Color("Primary Text"))
                        TextField("Tulis perjanjian disini", text: self.$agreements[index])
                            .textFieldStyle(RoundedBorderTextFieldStyle())
                        .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.white, lineWidth: 1))
                            
                        
                    }
                    Text("Deskripsi & Persetujuan Tambahan")
                    .font(.system(.body, design: .rounded))
                    .bold()
                    TextField("Tulis perjanjian disini", text: self.$description)
                    .textFieldStyle(RoundedBorderTextFieldStyle())
                    .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.white, lineWidth: 1))
                        
                    
                    Text("Terima Kasih!")
                    .font(.system(.title, design: .rounded))
                    .bold()
                    
                    Text("Terima kasih sudah mengisi seluruh informasi mengenai term & agreement shelter kalian. Terms & Agreement shelter kalian akan segera kami periksa dan ulas secepat mungkin.")
                        .fixedSize(horizontal: false, vertical: true)
                  
                }
            .padding()
            }
        }
        .padding()
    .navigationBarTitle("Perjanjian Shelter")
    }
}

struct RescuerAgreement_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView{
            RescuerAgreement()
        }
        
    }
}
