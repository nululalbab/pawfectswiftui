//
//  RescuerProfile.swift
//  ImpawsibbleSwiftUI
//
//  Created by Najibullah Ulul Albab on 05/08/20.
//  Copyright © 2020 Najibullah Ulul Albab. All rights reserved.
//

import SwiftUI

struct RescuerProfile: View {
   var name: String = "SACC"
    var reason: String = "Surabaya Animal Care Center.  SACC adalah organisasi / komunitas nirlaba yang menyediakan tempat tinggal bagi anjing terlantar di Surabaya."
    var status: [Bool] = [true, true]
    
    var body: some View {
        ScrollView {
            VStack(alignment: .leading, spacing: 0) {
                ZStack(alignment: Alignment.bottom) {
                    Image("Adopter Picture")
                        .resizable()
                        .aspectRatio(contentMode: .fill)
                        .frame(height: 300, alignment: Alignment.top)
                        .offset(y:-50)
                        .clipped()
                    
                    Rectangle()
                        .frame(height: 32)
                        .cornerRadius(20)
                        .offset(y: 16)
                        .foregroundColor(.white)
                }
                
                VStack(alignment: .leading, spacing: 16){
                    Text(self.name)
                        .font(.system(.largeTitle, design: .rounded))
                        .bold()
                        .foregroundColor(Color("Primary Text"))
                    
                    Text(self.reason)
                        .font(.system(.body, design: .rounded))
                        .fixedSize(horizontal: false, vertical: true)
                        .foregroundColor(Color("Primary Text"))
                    
                    NavigationLink(destination: Text("")){
                        RescuerProfileMenu(title: "Personal Info", filled: self.status[0])
                    }
                    .buttonStyle(PlainButtonStyle())
                    
                    NavigationLink(destination: RescuerAgreement()){
                        RescuerProfileMenu(title: "Perjanjian Shelter", filled: self.status[1])
                    }
                    .buttonStyle(PlainButtonStyle())
                    
                    
                }
                .padding(.horizontal, 16)
                .padding(.bottom, 16)
            }
        }
        .navigationBarTitle(
            Text("Rescuer Profile")
                .foregroundColor(.red)
                .font(.system(.largeTitle, design: .rounded))
                .foregroundColor(Color("Primary Text"))
        )
            .navigationBarItems(
                trailing: NavigationLink(destination: WishlistView()
                ) {
                    Image(systemName: "heart.fill")
                        .foregroundColor(.orange)
                }
                .buttonStyle(PlainButtonStyle())
        )
    }
}

struct RescuerProfile_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView{
            RescuerProfile()
        }
        
    }
}

struct RescuerProfileMenu: View {
    let title: String
    let filled: Bool
    
    var boxColor: Color {
        self.filled ? .orange : Color("Disabled")
    }
    
    var body: some View {
        VStack {
            HStack{
                Text(self.title)
                    .font(Font.system(size: 20, weight: .bold, design: .rounded))
                
                Spacer()
                Image(systemName: "chevron.right")
                    .font(Font.system(size: 20, weight: .bold, design: .rounded))
            }
            .padding(.vertical, 50)
            .padding(.horizontal)
            
        }
        .foregroundColor(.white)
        .background(self.boxColor)
        .cornerRadius(16)
    }
}
