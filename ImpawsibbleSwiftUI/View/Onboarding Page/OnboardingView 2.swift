//
//  OnboardingView.swift
//  ImpawsibbleSwiftUI
//
//  Created by Michael Randicha Gunawan Santoso on 31/07/20.
//  Copyright © 2020 Najibullah Ulul Albab. All rights reserved.
//

import SwiftUI

struct OnboardingView: View {
    @EnvironmentObject var settings: UserSettings
    
    var body: some View {
        Button(action: {
            self.settings.onBoardingFinished = true
        }) {
            Text("Finish Onboarding")
                .padding()
                .background(Color.green)
                .foregroundColor(.white)
                .cornerRadius(25)
        }
    }
}

struct OnboardingView_Previews: PreviewProvider {
    static var previews: some View {
        OnboardingView()
    }
}
