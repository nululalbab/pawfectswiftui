//
//  OnboardingView.swift
//  ImpawsibbleSwiftUI
//
//  Created by Michael Randicha Gunawan Santoso on 31/07/20.
//  Copyright © 2020 Najibullah Ulul Albab. All rights reserved.
//

import SwiftUI
import SwiftUIPager

struct OnboardingView: View {
    @EnvironmentObject var settings: UserSettings
    @State var currentPage: Int = 0
    var data: [OnboardingData] = OnboardingData.getData()
    
    var body: some View {
        VStack {
            Pager(page: self.$currentPage, data: data) { data in
                VStack(spacing: 18) {
                    Image("Onboarding \(self.currentPage + 1)")
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(height: 300)
                    
                    Text(data.title)
                        .font(.system(.largeTitle, design: .rounded))
                        .bold()
                        .foregroundColor(Color("Primary Text"))
                        .frame(width: UIScreen.main.bounds.width * 0.8, alignment: .leading)
                        
                    
                    Text(data.subtitle)
                        .font(.system(.body, design: .rounded))
                        .bold()
                        .foregroundColor(Color("Primary Text"))
                        .frame(width: UIScreen.main.bounds.width * 0.8, alignment: .leading)
                }
                .frame(minWidth: 0, maxWidth: .infinity, alignment: .center)
                .background(Color.white)
            }
            .frame(height: UIScreen.main.bounds.height * 0.75)
            
            HStack {
                PageControl(numberOfPages: self.data.count, currentPageIndex: self.$currentPage)
                    .transition(.identity)
            }
            .animation(.easeInOut)
            .frame(width: UIScreen.main.bounds.width * 0.8)
            Spacer()
                                Button(action: {
                withAnimation {
                    self.settings.onBoardingFinished = true
                }
            }) {
                Text("Yuk Mulai Adopsi")
                .foregroundColor(.white)
                        
                }
                   .frame(minWidth: 0, maxWidth: .infinity)
                                .padding()
                .background(Color.orange)
                .cornerRadius(16)
                                .padding(.horizontal)
            
            .transition(.scale)
        }
    }
}

struct OnboardingView_Previews: PreviewProvider {
    static var previews: some View {
        OnboardingView()
    }
}
