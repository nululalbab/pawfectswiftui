//
//  UserSettings.swift
//  ImpawsibbleSwiftUI
//
//  Created by Michael Randicha Gunawan Santoso on 31/07/20.
//  Copyright © 2020 Najibullah Ulul Albab. All rights reserved.
//

import Foundation
import Combine

class UserSettings: ObservableObject {
    @Published var onBoardingFinished: Bool {
        didSet {
            UserDefaults.standard.set(onBoardingFinished, forKey: "onBoardingFinished")
        }
    }
    
    @Published var userType: String {
        didSet {
            UserDefaults.standard.set(userType, forKey: "userType")
        }
    }
    
    init() {
        self.onBoardingFinished = UserDefaults.standard.bool(forKey: "onBoardingFinished")
        self.userType = UserDefaults.standard.string(forKey: "userType") ?? ""
    }
    
    //For Testing Purposes
    func reset() {
        UserDefaults.standard.removeObject(forKey: "onBoardingFinished")
        UserDefaults.standard.removeObject(forKey: "userType")
        self.onBoardingFinished = UserDefaults.standard.bool(forKey: "onBoardingFinished")
        self.userType = UserDefaults.standard.string(forKey: "userType") ?? ""
    }
}
