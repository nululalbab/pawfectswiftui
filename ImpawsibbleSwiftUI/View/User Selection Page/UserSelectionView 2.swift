//
//  UserSelectionView.swift
//  ImpawsibbleSwiftUI
//
//  Created by Michael Randicha Gunawan Santoso on 31/07/20.
//  Copyright © 2020 Najibullah Ulul Albab. All rights reserved.
//

import SwiftUI

struct UserSelectionView: View {
    @EnvironmentObject var settings: UserSettings
    @State var selection: String = "";
    
    var body: some View {
        VStack {
            Spacer()
            VStack(spacing: 16) {
                Text("Who Are You?")
                    .font(.system(.largeTitle, design: .rounded))
                    .foregroundColor(Color("Primary Text"))
                    .bold()
                
                HStack (spacing: 16) {
                    UserType(selection: self.$selection, type: "Rescuer")
                    
                    UserType(selection: self.$selection, type: "Adopter")
                }
            }
            Spacer()
            HStack {
                Button(action: {
                    self.settings.userType = self.selection
                }) {
                    Text("Let's Go")
                        .frame(minWidth: 0, maxWidth: .infinity)
                        .padding()
                        .foregroundColor(.white)
                        .background(self.selection != "" ? Color.orange : Color.gray)
                }
                .buttonStyle(PlainButtonStyle())
                .cornerRadius(16)
                .padding(.horizontal)
                .disabled(self.selection == "")
            }
        }
    }
}

struct UserSelectionView_Previews: PreviewProvider {
    static var previews: some View {
        UserSelectionView()
    }
}

struct UserType: View {
    @Binding var selection: String
    var type: String
    var body: some View {
        Button(action: {
            self.selection = self.type
        }) {
            VStack {
                Image(self.selection == type ? "\(type) Icon Color" : "\(type) Icon BW")
                    .resizable().frame(width: UIScreen.main.bounds.width * 0.35, height: UIScreen.main.bounds.width * 0.35)
                    .background(Color.white)
                    .cornerRadius(24)
                
                Text(type)
                    .font(.system(size: 16, weight: .bold, design: .rounded))
                    .foregroundColor(.white)
            }
            .padding()
            .background(self.selection == type ? Color.orange : Color.gray)
            .cornerRadius(24)
        }
        .buttonStyle(PlainButtonStyle())
    }
}
