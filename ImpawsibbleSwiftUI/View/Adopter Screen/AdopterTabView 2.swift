//
//  AdopterTabView.swift
//  ImpawsibbleSwiftUI
//
//  Created by Michael Randicha Gunawan Santoso on 31/07/20.
//  Copyright © 2020 Najibullah Ulul Albab. All rights reserved.
//

import SwiftUI

struct AdopterTabView: View {
    @EnvironmentObject var settings: UserSettings
    
    var body: some View {
        TabView {
            NavigationView {
                Text("Match")
                .navigationBarItems(trailing:
                    Button(action: {
                        self.settings.reset()
                    }) {
                    Text("Reset")
                    }
                )
            }
            .tabItem {
                Image(systemName: "hare.fill")
                Text("Adopt")
            }
            
            NavigationView {
                Text("Adopter Profile")
            }
            .tabItem {
                Image(systemName: "person.fill")
                Text("Profile")
            }
        }
        .accentColor(.orange)
    }
}

struct AdopterTabView_Previews: PreviewProvider {
    static var previews: some View {
        AdopterTabView()
    }
}
