//
//  KnowledgeQuestionView.swift
//  ImpawsibbleSwiftUI
//
//  Created by Michael Randicha Gunawan Santoso on 03/08/20.
//  Copyright © 2020 Najibullah Ulul Albab. All rights reserved.
//

import SwiftUI
import SwiftUIPager

struct KnowledgeQuestionView: View {
     
    @State var currentPage: Int = 0
    @State var questions: [KnowledgeQuestion] = KnowledgeQuestion.getQuestions()
    var body: some View {
        Color("Secondary")
            .edgesIgnoringSafeArea(.vertical) // Ignore just for the color
            .overlay(
                Pager(page: $currentPage, data: questions, content: { _ in
                    ZStack {
                        Text("\(self.currentPage)")
                        KnowledgeQuestionShadow()
                        KnowledgeQuestionCard(question: self.$questions[self.currentPage], page: self.$currentPage, maxPage: self.questions.count)
                    }
                    .offset(y: -50)
                })
        )
    }
}

struct KnowledgeQuestionView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            KnowledgeQuestionView()
        }
    }
}

struct KnowledgeQuestionShadow: View {
    var body: some View {
        VStack {
            EmptyView()
        }
        .frame(
            width: UIScreen.main.bounds.width * 0.8,
            height: UIScreen.main.bounds.height * 0.5)
            .background(Color.black)
            .opacity(0.1)
            .cornerRadius(30)
            .offset(y: 30)
    }
}

struct KnowledgeQuestionCard: View {
    @EnvironmentObject var setting: UserSettings
    @Binding var question: KnowledgeQuestion
    @Binding var page: Int
    let maxPage: Int
    
    var body: some View {
        VStack {
            VStack(spacing: 0) {
                HStack(alignment: .bottom) {
                    Text("\(String(format: "%02d", self.page + 1))")
                        .font(.system(.largeTitle, design: .rounded))
                        .bold()
                        .foregroundColor(Color("Primary Text"))
                    
                    Text("of \(self.maxPage)")
                        .font(.system(size: 20, weight: .bold, design: .rounded))
                        .bold()
                        .foregroundColor(.secondary)
                        .padding(.bottom, 4)
                    
                    Spacer()
                }
                .padding(Edge.Set([.leading, .top]),8)
                
                HStack {
                    Rectangle()
                        .frame(width: UIScreen.main.bounds.width * 0.1, height: 5)
                        .foregroundColor(Color.orange)
                        .cornerRadius(5)
                    Spacer()
                }
                .padding(.leading, 8)
            }
            
            Spacer()
            VStack(spacing: 16) {
                Text(question.question)
                    .font(.system(size: 22, weight: .bold, design: .rounded))
                    .bold()
                    .foregroundColor(Color("Primary Text"))
                    .multilineTextAlignment(.center)
                    .fixedSize(horizontal: false, vertical: true)
                
                if(question.options.isEmpty) {
                    TextArea(text: $question.answer)
                    .padding(8)
                    .overlay(
                        RoundedRectangle(cornerRadius: 16)
                            .stroke(Color("Disabled"), lineWidth: 1)
                    )
                } else {
                    VStack(spacing: 16) {
                        ForEach(question.options, id: \.self) { option in
                            Button(action: {
                                self.question.answer = option
                            }) {
                                HStack {
                                    Text(option)
                                    .font(.system(.body, design: .rounded))
                                    .bold()
                                    .foregroundColor(.white)
                                    
                                    Spacer()
                                }
                                .padding()
                                .frame(minWidth: 0, maxWidth: .infinity, alignment: .leading)
                                .background(self.question.answer == option ? Color.orange : Color("Disabled"))
                                .cornerRadius(16)
                            }
                            .buttonStyle(PlainButtonStyle())
                        }
                    }
                }
            }.padding()
            Spacer()
            
            HStack {
                if(self.page > 0) {
                    Button(action: {
                        self.page = self.page - 1
                    }) {
                        Text("Back") .font(.system(.body, design: .rounded))
                            .foregroundColor(.white)
                            .padding(.vertical, 8)
                            .padding(.horizontal)
                            .background(Color.orange)
                            .cornerRadius(10)
                    }
                    .buttonStyle(PlainButtonStyle())
                }
                
                Spacer()
                
                if(self.page <= self.maxPage - 1) {
                    if(self.page < self.maxPage - 1) {
                        Button(action: {
                            self.page = self.page + 1
                        }) {
                            Text("Next") .font(.system(.body, design: .rounded))
                                .foregroundColor(.white)
                                .padding(.vertical, 8)
                                .padding(.horizontal)
                                .background(Color.orange)
                                .cornerRadius(10)
                        }
                        .buttonStyle(PlainButtonStyle())
                    }else {
                        NavigationLink(destination: PetAssessmentView()) {
                            Text("Finish") .font(.system(.body, design: .rounded))
                            .foregroundColor(.white)
                            .padding(.vertical, 8)
                            .padding(.horizontal)
                            .background(Color.orange)
                            .cornerRadius(10)
                        }
                        .simultaneousGesture(TapGesture().onEnded{
                            self.setting.userAssessment = true
                        })
                    }
                }
            }
            .padding()
        }
        .padding()
        .frame(
            width: UIScreen.main.bounds.width * 0.9,
            height: UIScreen.main.bounds.height * 0.5)
            .background(Color.white)
            .cornerRadius(30)
    }
}
