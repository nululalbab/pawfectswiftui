//
//  userprofile.swift
//  ImpawsibbleSwiftUI
//
//  Created by wahyujus on 07/08/20.
//  Copyright © 2020 Najibullah Ulul Albab. All rights reserved.
//

import SwiftUI

struct Users: Identifiable {
    let id = UUID()
    var name:String
    var item:String
}

struct PersonalInfoView: View {
    @EnvironmentObject var setting: UserSettings
    @State var user = [
    Users(name: "Nama", item: "Susi Jayanti"),
    Users(name: "Tujuan", item: "Tujuan."),
    Users(name: "Umur", item: "32"),
    Users(name: "Jenis Kelamin", item: "Female"),
    Users(name: "Alamat", item: "Citraland, Eastwood AB/35.\nSurabaya, Jawa Timur"),
    Users(name: "Email", item: "susi.jayanti@icloud.com"),
    Users(name: "Telepon", item: "+6281235437xxx"),
    Users(name: "Whatsapp", item: "+6281235437xxx"),
    Users(name: "Status", item: "Menikah"),
    Users(name: "Pekerjaan", item: "Entrepreneur")
    ]
    
    @State var showSheet = false
    
    var body: some View {
      
        VStack {
            
            VStack{
                Image("Adopter Picture")
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(width: 150, height: 150, alignment: .center)
                    .clipped()
                    .cornerRadius(150)
                    .shadow(radius: 3)
                
                Text("Change Profile Photo")
                    .font(.subheadline)
                    .fontWeight(.medium)
                    .foregroundColor(.orange)
                    .padding(.bottom)
                
            }
            
            List(user){
                
                index in
                
                userRow(user: index, showSheet: self.showSheet)
                
                
            }
            NavigationLink(destination: KnowledgeQuestionView()){
                Text("Simpan")
                    .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 50)
                    
                    
                    .background(Color.orange)
                    .foregroundColor(.white)
                    .cornerRadius(16)
                    .padding()
            }
            .simultaneousGesture(TapGesture().onEnded{
                self.setting.userProfile = true
                })

            
            
            
        }
    .navigationBarTitle("Info Personal")
    }
}

struct PersonalInfoView_Preview: PreviewProvider {
    static var previews: some View {
        NavigationView{
             PersonalInfoView()
        }.navigationBarTitle(Text("Info Personal"))
       
    }
}

struct userRow: View {
    @State var user: Users
    @State var showSheet : Bool
    
    var body: some View {
        
        
            
            HStack(spacing: 0) {
                
                Text("\(user.name)")
                    .foregroundColor(.gray)
            
            Spacer(minLength: 0)
            
            Button(action: {self.showSheet.toggle()}, label: {
                Text("\(user.item)")
            })
        }.sheet(isPresented: $showSheet, content: {
            DetailView(selectedItem: self.$user.item, showSheet: self.$showSheet)
        })
            
            
        
        
    }
}

struct DetailView: View {
    
    @Binding var selectedItem: String
    @Binding var showSheet : Bool
    
    var body: some View {
        Form {
                    
                    HStack (spacing:16) {
                        Text("Name")
                        TextField("\(selectedItem)", text: $selectedItem)
                    }
                    
                    Button(action: {
                        
                        
                        self.showSheet = false
                        
                        
                    }, label: {
                        Text("Done")
                    })
                }.padding(16)
    }
}

struct userEdit: View {
    
    @State var user = [
    Users(name: "Nama", item: "Susi Jayanti"),
    Users(name: "Tujuan", item: "Tujuan."),
    Users(name: "Umur", item: "32"),
    Users(name: "JenisKelamin", item: "Female"),
    Users(name: "Alamat", item: "Citraland, Eastwood AB/35.\nSurabaya, Jawa Timur"),
    Users(name: "Email", item: "susi.jayanti@icloud.com"),
    Users(name: "Telepon", item: "+6281235437xxx"),
    Users(name: "Whatsapp", item: "+6281235437xxx"),
    Users(name: "Status", item: "Menikah"),
    Users(name: "Pekerjaan", item: "Entrepreneur")
    ]
    
    var body: some View {
        
        VStack {
                
                VStack{
                    Image(systemName: "tortoise.fill")
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(width: 150, height: 150, alignment: .center)
                        .clipped()
                        .cornerRadius(150)
                        .shadow(radius: 3)
                    
                    Text("Change Profile Photo")
                        .font(.subheadline)
                        .fontWeight(.medium)
                        .foregroundColor(.orange)
                        .padding(.bottom)
                }
                
                List(user){
                    
                    index in
                    
                    userRowEdit(user: index)
                    
                }.navigationBarTitle(Text("Info Personal"))
            
        }
        
    }
}


struct userRowEdit: View {
    var user: Users
    @State var name = ""
    
    var body: some View {
        
        
            
            VStack(alignment: .leading, spacing: 0) {
                
                HStack {
                    Text("\(user.name)")
                        .foregroundColor(.gray)
                    
                    Spacer()
                        
                    TextField(user.item, text: $name)
                }
        }
        
        
    }
}
