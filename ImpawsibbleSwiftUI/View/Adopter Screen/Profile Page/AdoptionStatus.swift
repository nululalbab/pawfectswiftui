//
//  AdoptionStatus.swift
//  ImpawsibbleSwiftUI
//
//  Created by Najibullah Ulul Albab on 12/08/20.
//  Copyright © 2020 Najibullah Ulul Albab. All rights reserved.
//

import SwiftUI

struct AdoptionStatus: View {
    let pet: [String:String]
    

    
    var body: some View {
        ScrollView{
            VStack{
                AdoptionProgress(status: self.pet["status"]!, title: convertStatusToTitle(status: self.pet["status"]!), subtitle: convertStatusToSubtitle(status: self.pet["status"]!))
                    
                NavigationLink(destination: AnimalProfile(pet: self.pet)){
                        Text("Lihat profil Hewan")
                            .frame(minWidth: 0, maxWidth: .infinity)
                            .padding()
                            .font(.system(.body, design: .rounded))
                            .background(Color.orange)
                            .foregroundColor(.white)
                            .cornerRadius(16)
                    }
                .padding()
                }
            .padding()
                .background(Color("Secondary"))
        }
        
        
    }
}

struct AdoptionStatus_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView{
            AdoptionStatus(pet: ["name": "Nugget", "type": "Border Collie", "location": "Surabaya", "imageName": "Nugget", "status": "Waiting List"])
        }
        
    }
}

struct AdoptionProgress: View {
    var status: String
    var title: String
    var subtitle: String
    var body: some View {
        VStack{
           
            if status == "Waiting List" {
                VStack(spacing: 0){
                    HStack{
                        VStack(alignment: .leading, spacing: 0){
                            Image("screening")
                            .resizable()
                            .frame(width: 100, height: 100)
                            .scaledToFit()
                            Rectangle()
                            .fill(Color.white)
                            .frame(width: 5, height: 50)
                        }
                        Spacer()
                    }
                    .padding(.horizontal, 50)
                    
                    HStack{
                        Image("interview inactive")
                       .resizable()
                       .frame(width: 100, height: 100)
                       .scaledToFill()
                        Rectangle()
                        .fill(Color.white)
                        .frame(width: 100, height: 5)
                        Image("home visit inactive")
                        .resizable()
                        .frame(width: 100, height: 100)
                        .scaledToFit()
                       
                    }
                    HStack{
                        
                        Spacer()
                        VStack{
                            Rectangle()
                            .fill(Color.white)
                            .frame(width: 5, height: 50)
                            Image("done yay inactive")
                            .resizable()
                            .frame(width: 100, height: 100)
                            .scaledToFit()
                        }
                    }
                    
                    
                }
            } else if status == "Reviewing" {
                VStack(spacing: 0){
                    HStack{
                        VStack(alignment: .leading, spacing: 0){
                            Image("screening")
                            .resizable()
                            .frame(width: 100, height: 100)
                            .scaledToFit()
                            Rectangle()
                            .fill(Color.orange)
                            .frame(width: 5, height: 50)
                        }
                        Spacer()
                    }
                    .padding(.horizontal, 50)
                    
                    HStack{
                        Image("interview")
                       .resizable()
                       .frame(width: 100, height: 100)
                       .scaledToFill()
                        Rectangle()
                        .fill(Color.white)
                        .frame(width: 100, height: 5)
                        Image("home visit inactive")
                        .resizable()
                        .frame(width: 100, height: 100)
                        .scaledToFit()
                       
                    }
                    HStack{
                        
                        Spacer()
                        VStack{
                            Rectangle()
                            .fill(Color.white)
                            .frame(width: 5, height: 50)
                            Image("done yay inactive")
                            .resizable()
                            .frame(width: 100, height: 100)
                            .scaledToFit()
                        }
                    }
                    
                    
                }
            }
            else if status == "Home Visit" {
                VStack(spacing: 0){
                    HStack{
                        VStack(alignment: .leading, spacing: 0){
                            Image("screening")
                            .resizable()
                            .frame(width: 100, height: 100)
                            .scaledToFit()
                            Rectangle()
                            .fill(Color.orange)
                            .frame(width: 5, height: 50)
                        }
                        Spacer()
                    }
                    .padding(.horizontal, 50)
                    
                    HStack{
                        Image("interview")
                       .resizable()
                       .frame(width: 100, height: 100)
                       .scaledToFill()
                        Rectangle()
                        .fill(Color.orange)
                        .frame(width: 100, height: 5)
                        Image("home visit")
                        .resizable()
                        .frame(width: 100, height: 100)
                        .scaledToFit()
                       
                    }
                    HStack{
                        
                        Spacer()
                        VStack{
                            Rectangle()
                            .fill(Color.white)
                            .frame(width: 5, height: 50)
                            Image("done yay inactive")
                            .resizable()
                            .frame(width: 100, height: 100)
                            .scaledToFit()
                        }
                    }
                    
                    
                }
            }
            else if status == "Success" {
               VStack(spacing: 0){
                    HStack{
                        VStack(alignment: .leading, spacing: 0){
                            Image("screening")
                            .resizable()
                            .frame(width: 100, height: 100)
                            .scaledToFit()
                            Rectangle()
                            .fill(Color.orange)
                            .frame(width: 5, height: 50)
                        }
                        Spacer()
                    }
                    .padding(.horizontal, 50)
                    
                    HStack{
                        Image("interview")
                       .resizable()
                       .frame(width: 100, height: 100)
                       .scaledToFill()
                        Rectangle()
                        .fill(Color.orange)
                        .frame(width: 100, height: 5)
                        Image("home visit")
                        .resizable()
                        .frame(width: 100, height: 100)
                        .scaledToFit()
                       
                    }
                    HStack{
                        
                        Spacer()
                        VStack{
                            Rectangle()
                            .fill(Color.orange)
                            .frame(width: 5, height: 50)
                            Image("done yay")
                            .resizable()
                            .frame(width: 100, height: 100)
                            .scaledToFit()
                        }
                    }
                    
                    
                }
            }
            else if status == "Declined" {
               
                    Image("done nay")
                .resizable()
                .frame(width: 100, height: 100)
                .scaledToFit()
            }
            Spacer()
            Text(self.title)
                .font(.system(.largeTitle, design: .rounded))
                .bold()
                .foregroundColor(Color("Primary Text"))
                .padding()
            Text(self.subtitle)
                .font(.system(.body, design: .rounded))
                .foregroundColor(Color("Primary Text"))
            .lineLimit(nil)
                .padding()
            
        }
    }
}



func convertStatusToTitle(status: String)-> String{
   if status == "Waiting List" {
        return "Yay!"
    }else if status == "Declined" {
        return "Maaf!"
    }else if status == "Reviewing" {
        return "Hore!"
    }else if status == "Home Visit" {
        return "Kabar Gembira!"
    }else if status == "Success" {
        return "Selamat!"
    }
    
    return status
}

func convertStatusToSubtitle(status: String)-> String{
   if status == "Waiting List" {
        return "Sekarang kamu masuk tahap seleksi. Rescuer akan memeriksa pendaftaran kamu."
    }else if status == "Declined" {
        return "Kamu belum memenuhi beberapa persyaratan di salah satu tahap seleksi. Coba lagi di lain waktu ya!"
    }else if status == "Reviewing" {
        return "Kamu masuk ke tahap wawancara. Perhatikan kontak untuk janji jadwal dan lokasi shelter ya!"
    }else if status == "Home Visit" {
        return "Wawancara kamu berhasil loh! Rescuer akan mengontak kamu untuk kunjungan rumah."
    }else if status == "Success" {
        return "Kamu berhasil mengadopsi sahabatmu! Segera buat janji temu dengan rescuer ya."
    }
    
    return status
}
