//
//  KnowledgeVIew.swift
//  ImpawsibbleSwiftUI
//
//  Created by Michael Randicha Gunawan Santoso on 03/08/20.
//  Copyright © 2020 Najibullah Ulul Albab. All rights reserved.
//

import SwiftUI

struct KnowledgeView: View {
    var knowledgeDatas: [[String:String]] = [
        ["question": "Where do you live?", "answer": ""],
        ["question": "Does your home have a yard?", "answer": "Lorem Ipsum 2"],
        ["question": "Who do you live with?", "answer": "Lorem Ipsum 3"],
        ["question": "Have you ever had a pet?", "answer": "Lorem Ipsum 4"],
        ["question": "Do you have pets now?", "answer": "Lorem Ipsum 5"],
        ["question": "Did you know about the provision of vaccinations for pets based on age?", "answer": "Lorem Ipsum 6"],
        ["question": "Do you know about pet diseases?", "answer": "Lorem Ipsum 7"],
        ["question": "Who will take care of the pet?", "answer": "Lorem Ipsum 8"],
        ["question": "What care will you provide?", "answer": "Lorem Ipsum 9"],
        ["question": "What will you do with your pet when you travel?", "answer": "Lorem Ipsum 10"],
        ["question": "Do all family members/people you live with agree you adopt pets?", "answer": "Lorem Ipsum 11"],
    ]
    
    var body: some View {
        List(knowledgeDatas, id: \.self) { data in
            KnowledgeData(data: data)
        }
        .listStyle(GroupedListStyle())
        .navigationBarTitle("Knowledge & Care")
    }
}

struct KnowledgeView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            KnowledgeView()
        }
    }
}

struct KnowledgeData: View {
    @State var expanded: Bool = false
    var data: [String:String]
    
    var body: some View {
        Button(action: {
            self.expanded = !self.expanded
        }) {
            VStack(alignment: HorizontalAlignment.leading) {
                HStack {
                    Text(self.data["question"]!)
                        .font(.system(size: 20, weight: .bold, design: .rounded))
                        .foregroundColor(Color("Primary Text"))
                    
                    Spacer()
                    
                    Image(systemName: self.expanded ? "chevron.up" : "chevron.down")
                        .font(Font.system(size: 18, weight: .bold, design: .rounded))
                        .foregroundColor(.orange)
                        .padding(.leading, 8)
                }
                if(expanded) {
                    Text(self.data["answer"]!.isEmpty ? "You have not filled out the questionnaire" : self.data["answer"]!)
                        .multilineTextAlignment(.leading)
                        .foregroundColor(Color("Primary Text"))
                }
            }
            .padding(.vertical, 8)
            .background(Color.white)
        }
        .buttonStyle(PlainButtonStyle())
    }
}
