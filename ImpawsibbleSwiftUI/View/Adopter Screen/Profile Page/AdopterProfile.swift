//
//  AdopterProfile.swift
//  ImpawsibbleSwiftUI
//
//  Created by Michael Randicha Gunawan Santoso on 29/07/20.
//  Copyright © 2020 Najibullah Ulul Albab. All rights reserved.
//

import SwiftUI

struct AdopterProfile: View {
    @EnvironmentObject var setting: UserSettings
    var name: String = "Susi Jayanti"
    var reason: String = "The reason of my adoption is I want some company. My husband is a pilot, so most of the time I stayed alone at home. I want my home more lively."
    var status: [Bool] = [false, false, false]
    
    var body: some View {
        ScrollView {
            VStack(alignment: .leading, spacing: 0) {
                ZStack(alignment: Alignment.bottom) {
                    Image("Adopter Picture")
                        .resizable()
                        .aspectRatio(contentMode: .fill)
                        .frame(height: 300, alignment: Alignment.top)
                        .offset(y:-50)
                        .clipped()
                    
                    Rectangle()
                        .frame(height: 32)
                        .cornerRadius(20)
                        .offset(y: 16)
                        .foregroundColor(.white)
                }
                
                VStack(alignment: .leading, spacing: 16){
                    Text(self.name)
                        .font(.system(.largeTitle, design: .rounded))
                        .bold()
                        .foregroundColor(Color("Primary Text"))
                    
                    Text(self.reason)
                        .font(.system(.body, design: .rounded))
                        .fixedSize(horizontal: false, vertical: true)
                        .foregroundColor(Color("Primary Text"))
                    
                    NavigationLink(destination: PersonalInfoView()){
                        AdopterProfileMenu(title: "Personal Info", filled: self.setting.userProfile, imageName: "info pribadi\(self.setting.userProfile ? "" : " inactive")")
                    }
                    .buttonStyle(PlainButtonStyle())
                    
                    NavigationLink(destination: PetAssessmentView()){
                        AdopterProfileMenu(title: "Pet Assessment", filled: self.setting.userAssessment, imageName: "pet assessment\(self.setting.userAssessment ? "" : " inactive")")
                    }
                    .buttonStyle(PlainButtonStyle())
                    
                    NavigationLink(destination: AdoptionHistoryView()){
                        AdopterProfileMenu(title: "Adoption History", filled: self.status[2], imageName: "riwayat adopsi inactive")
                    }
                    .buttonStyle(PlainButtonStyle())
                }
                .padding(.horizontal, 16)
                .padding(.bottom, 16)
            }
        }
        .navigationBarTitle(
            Text("Adopter Profile")
                .foregroundColor(.red)
                .font(.system(.largeTitle, design: .rounded))
                .foregroundColor(Color("Primary Text"))
        )
            
    }
}

struct AdopterProfile_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            AdopterProfile()
        }
    }
}

struct AdopterProfileMenu: View {
    let title: String
    let filled: Bool
    let imageName: String
    
    var boxColor: Color {
        self.filled ? .orange : Color("Disabled")
    }
    
    var body: some View {
        VStack {
            HStack{
                Text(self.title)
                    .font(Font.system(size: 20, weight: .bold, design: .rounded))
                
                Spacer()
                Image(self.imageName)
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(width: 100, height: 100)
                Image(systemName: "chevron.right")
                    .font(Font.system(size: 20, weight: .bold, design: .rounded))
            }
            .padding(.horizontal)
            
        }
        .foregroundColor(.white)
        .background(self.boxColor)
        .cornerRadius(16)
    }
}
