//
//  WishlistView.swift
//  ImpawsibbleSwiftUI
//
//  Created by Michael Randicha Gunawan Santoso on 02/08/20.
//  Copyright © 2020 Najibullah Ulul Albab. All rights reserved.
//

import SwiftUI

struct WishlistView: View {
    var wishlists: [[String:String]] = [
        ["name": "Nugget", "type": "Border Collie", "location": "Surabaya", "imageName": "Nugget"]
    ]
    
    var body: some View {
        ScrollView {
            VStack(spacing: 16) {
                ForEach(0 ..< self.wishlists.count) { index in
                    WishlistCard(wishlist: self.wishlists[index])
                }
                if(self.wishlists.isEmpty) {
                    VStack {
                        //Need to change the Image to Illustration Later
                        Image("Rescuer Icon BW")
                            .resizable()
                            .frame(
                                width: UIScreen.main.bounds.width * 0.4,
                                height: UIScreen.main.bounds.width * 0.4)
                        
                        Text("You haven’t put any pet\nto wishlist")
                            .font(.system(size: 18, weight: .bold, design: .rounded))
                            .foregroundColor(.secondary)
                            .multilineTextAlignment(.center)
                            .fixedSize(horizontal: false, vertical: true)
                    }
                    .frame(height: UIScreen.main.bounds.height)
                }
            }
            .padding()
        }
        .navigationBarTitle(Text("Wishlist"))
        .edgesIgnoringSafeArea(self.wishlists.isEmpty ? .top : [])
    }
}

struct WishlistView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            WishlistView()
        }
    }
}

struct WishlistCard: View {
    var wishlist: [String: String]
    
    var body: some View {
        ZStack (alignment: Alignment.bottom) {
            Image(self.wishlist["imageName"]!)
                .resizable()
                .aspectRatio(16/9, contentMode: .fit)
                .cornerRadius(16)
            VStack {
                VStack(alignment: HorizontalAlignment.leading) {
                    Text(self.wishlist["name"]!)
                        .font(.system(size: 16, weight: .bold, design: .rounded))
                        .foregroundColor(Color("Primary Text"))
                    
                    HStack {
                        Text(self.wishlist["type"]!)
                            .font(.system(.body, design: .rounded))
                            .foregroundColor(.secondary)
                        Spacer()
                        Text(self.wishlist["location"]!)
                            .font(.system(.body, design: .rounded))
                            .foregroundColor(.secondary)
                    }
                }
                .padding()
                .background(Color("Secondary"))
                .cornerRadius(16)
            }
            .padding(.horizontal)
            .offset(y: 20)
        }
        .padding(.bottom, 20)
    }
}
