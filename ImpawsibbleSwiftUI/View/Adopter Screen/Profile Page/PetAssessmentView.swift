//
//  PetAssessmentView.swift
//  ImpawsibbleSwiftUI
//
//  Created by Michael Randicha Gunawan Santoso on 11/08/20.
//  Copyright © 2020 Najibullah Ulul Albab. All rights reserved.
//

import SwiftUI

struct PetAssessmentView: View {
    let title: [String] = [
        "Jenis Hewan",
        "Ukuran",
        "Tahap Kehidupan",
        "Pemeliharaan"
    ]
    
    let result: [[String]] = [
        ["Kucing", "Anjing"],
        ["Mungil", "Kecil", "Sedang"],
        ["Dewasa"],
        ["Rendah", "Sedang"]
    ]
    
    var body: some View {
        ScrollView {
            VStack(alignment: .leading, spacing: 16){
                Text("Halo!")
                    .font(.system(.largeTitle, design: .rounded))
                    .bold()
                    .foregroundColor(Color("Primary Text"))
                
                Text("Berdasarkan jawaban kuis kamu, kamu paling cocok mengadopsi hewan dengan kriteria seperti di bawah ini. ")
                    .font(.system(.body, design: .rounded))
                    .foregroundColor(Color("Primary Text"))
            }
            .frame(minWidth: 0, maxWidth: .infinity)
            .padding()
            .background(Color("Secondary"))
            .cornerRadius(20)
            .padding()
            
            Divider()
            
            ForEach(title.indices) { index in
                AssessmentTile(title: self.title[index], result: self.result[index])
            }
        }
        .navigationBarTitle(Text("Pet Assessment"))
            .navigationBarItems(
                trailing: NavigationLink(destination: KnowledgeQuestionView()) {
                Text("Kuis Ulang")
            })
    }
}

struct PetAssessmentView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            PetAssessmentView()
        }
    }
}

struct AssessmentTile: View {
    let title: String
    let result: [String]
    
    var body: some View {
        HStack {
            Text(title)
                .font(.system(.body, design: .rounded))
                .bold()
                .foregroundColor(Color("Primary Text"))
                .frame(minWidth: 0, maxWidth: UIScreen.main.bounds.width * 0.3, alignment: .leading)
            
            Spacer()
            
            VStack {
                ScrollView(.horizontal, showsIndicators: false) {
                    HStack {
                        ForEach(self.result, id: \.self) { option in
                            Text(option)
                                .font(.system(.body, design: .rounded))
                                .foregroundColor(.white)
                                .bold()
                                .padding(12)
                                .background(Color.orange)
                                .cornerRadius(16)
                        }
                    }
                }
                
                Divider()
            }
            .frame(minWidth: 0, maxWidth: UIScreen.main.bounds.width * 0.8, alignment: .center)
        }
        .padding([.horizontal])
    }
}
