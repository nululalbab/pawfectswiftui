//
//  PetFilter.swift
//  ImpawsibbleSwiftUI
//
//  Created by wahyujus on 05/08/20.
//  Copyright © 2020 Najibullah Ulul Albab. All rights reserved.
//

import SwiftUI

struct PetFilterView: View {
    //buttons
    @State var checked1: [Bool] = [false, false, false, false]
    @State var checked2: [Bool] = [false, false, false]
    @State var checked3: [Bool] = [false, false, false, false]
    @State var checked4: [Bool] = [false, false, false]
    @State var checked5: [Bool] = [false, false, false, false, false, false]
    @State var checked6: [Bool] = [false, false, false, false, false, false]
    @State var checked7: [Bool] = [false, false, false]
    
    var animalType: [String] = ["Cat","Dog","Other","All"]
    var animalGender: [String] = ["Male","Female","All"]
    var animalSize: [String] = ["Toy","Small","Med","Big"]
    var animalLifeStage: [String] = ["Puppy","Adult","Elder"]
    var animalPersonality: [String] = ["Friendly","Affectionate","Free Spirit","Aloof","Clever","Shy"]
    var animalBehaviour: [String] = ["Playful","Talkative","Lazy","Territorial","Mischievous","Active"]
    var animalMaintenance: [String] = ["Low","Med","High"]
    
    var body: some View {
        ScrollView {
            VStack(spacing: 16) {
                CheckboxGroup(checked: self.$checked1, title: "Animal Type", type: self.animalType)
                
                CheckboxGroup(checked: self.$checked2, title: "Gender", type: self.animalGender
                )
                
                CheckboxGroup(checked: self.$checked3, title: "Size", type: self.animalSize
                )
                
                CheckboxGroup(checked: self.$checked4, title: "Life Stage", type: self.animalLifeStage
                )
                
                CheckboxGroupImage(checked: self.$checked5, title: "Personality", type: self.animalPersonality)
                
                CheckboxGroupImage(checked: self.$checked6, title: "Behaviour", type: self.animalBehaviour)
                
                CheckboxGroup(checked: self.$checked7, title: "Maintenance", type: self.animalMaintenance
                )
                
                NavigationLink(destination: AdopterProfile()){
                    Text("Save")
                    .frame(minWidth: 0, maxWidth: .infinity)
                    .padding()
                    .background(Color.orange)
                    .foregroundColor(.white)
                    .cornerRadius(16)
                }
                .buttonStyle(PlainButtonStyle())
            }
            .padding()
            .padding(.bottom)
        }
        .navigationBarTitle(Text("Pet Filter"))
    }
}

struct PetFilterView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            PetFilterView()
        }
    }
}

struct CheckboxFilter: View {
    @Binding var checked: Bool
    var animalFilter: String
    var body: some View {
        Button(action: {
            self.checked.toggle()
        }) {
            FilterButton(buttonText: self.animalFilter, buttonColor: .orange, active: self.checked)
        }
        .buttonStyle(PlainButtonStyle())
    }
}

struct CheckboxFilterImage: View {
    @Binding var checked: Bool
    var animalFilter: String
    var body: some View {
        Button(action: {
            self.checked.toggle()
        }) {
            VStack{
                Image(systemName: self.checked ? "tortoise.fill" : "tortoise")
                    .foregroundColor(self.checked ? .orange : .gray)
                    .frame(width: 40, height: 40, alignment: .center)
                FilterButton(buttonText: self.animalFilter, buttonColor: .orange, active: self.checked)
            }
        }
    }
}

struct FilterButton: View {
    
    var buttonText = "My Button"
    var buttonColor = Color.orange
    var active = false
    
    var body: some View {
        Text(buttonText)
            .bold()
            .foregroundColor(.white)
            .frame(minWidth: 0,
                   maxWidth: .infinity, minHeight: 30,
                   maxHeight: 40,
                   alignment: .center)
            .background(self.active ? buttonColor : Color.gray)
            .cornerRadius(10)
        
    }
}

struct CheckboxGroup: View {
    @Binding var checked: [Bool]
    let title: String
    let type: [String]
    
    var body: some View {
        VStack(alignment: .leading, spacing: 8) {
            Text(self.title)
                .bold()
            HStack{
                
                ForEach(self.type.indices){ index in
                    CheckboxFilter(checked: self.$checked[index], animalFilter: self.type[index])
                }
            }
        }
    }
}

struct CheckboxGroupImage: View {
    @Binding var checked: [Bool]
    let title: String
    let type: [String]
    var length: Int = 3
    
    var loop: Int {
        Int(ceil(Double(type.count) / Double(length)))
    }
    
    func arrLength(loop: Int) -> Int {
        loop == self.loop - 1 ? type.count : (loop * length) + length
    }
    
    var body: some View {
        VStack(alignment: .leading) {
            Text("Behaviour").bold()
            ForEach(0..<self.loop) { i in
                HStack{
                    ForEach((i * self.length) ..< self.arrLength(loop: i)){ j in
                        CheckboxFilterImage(checked: self.$checked[j], animalFilter: self.type[j])
                    }
                }
            }
        }
    }
}
