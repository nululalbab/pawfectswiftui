//
//  AdoptionHistoryView.swift
//  ImpawsibbleSwiftUI
//
//  Created by Michael Randicha Gunawan Santoso on 02/08/20.
//  Copyright © 2020 Najibullah Ulul Albab. All rights reserved.
//

import SwiftUI

struct AdoptionHistoryView: View {
    var pets: [[String:String]] = [
        ["name": "Nugget", "type": "Border Collie", "location": "Surabaya", "imageName": "Nugget", "status": "Waiting List"],
        ["name": "Julie", "type": "Border Collie", "location": "Surabaya", "imageName": "Julie", "status": "Success"],
        ["name": "Becky", "type": "Border Collie", "location": "Surabaya", "imageName": "Becky", "status": "Reviewing"]
        ,
        ["name": "Sukti", "type": "Border Collie", "location": "Surabaya", "imageName": "Sukti", "status": "Home Visit"],
        ["name": "Cookie", "type": "Border Collie", "location": "Surabaya", "imageName": "Cookie", "status": "Declined"]
    ]
    
    var body: some View {
        ScrollView {
            VStack(spacing: 16) {
                ForEach(0 ..< self.pets.count) { index in
                    NavigationLink(destination: AdoptionStatus(pet: self.pets[index])){
                        AdoptionHistoryCard(pet: self.pets[index])
                    }
                .buttonStyle(PlainButtonStyle())
                }
                if(self.pets.isEmpty) {
                    VStack {
                        //Need to change the Image to Illustration Later
                        Image("Rescuer Icon BW")
                            .resizable()
                            .frame(
                                width: UIScreen.main.bounds.width * 0.4,
                                height: UIScreen.main.bounds.width * 0.4)
                        
                        Text("You don't have any\nadoption history")
                            .font(.system(size: 18, weight: .bold, design: .rounded))
                            .foregroundColor(.secondary)
                            .multilineTextAlignment(.center)
                            .fixedSize(horizontal: false, vertical: true)
                    }
                    .frame(height: UIScreen.main.bounds.height)
                }
            }
            .padding()
        }
        .navigationBarTitle(Text("Adoption History"))
        .edgesIgnoringSafeArea(self.pets.isEmpty ? .top : [])
    }
}

struct AdoptionHistoryView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            AdoptionHistoryView()
        }
    }
}

struct AdoptionHistoryCard: View {
    var pet: [String: String]
    
    func statusColor(_ statusOptional: String?) -> Color {
        if let status = statusOptional {
            if status == "Waiting List" {
                return .orange
            }else if status == "Declined" {
                return .red
            }else if status == "Reviewing" {
                return .orange
            }else if status == "Home Visit" {
                return .orange
            }else if status == "Success" {
                return .green
            }
        }
        return .secondary
    }
    
    var body: some View {
        ZStack (alignment: Alignment.bottom) {
            Image(self.pet["imageName"]!)
                .resizable()
                .aspectRatio(16/9, contentMode: .fit)
                .cornerRadius(16)
            VStack {
                VStack(alignment: HorizontalAlignment.leading) {
                    Text(self.pet["name"]!)
                        .font(.system(size: 16, weight: .bold, design: .rounded))
                        .foregroundColor(Color("Primary Text"))
                    
                    HStack {
                        Text(self.pet["type"]!)
                            .font(.system(.body, design: .rounded))
                            .foregroundColor(.secondary)
                        Spacer()
                        Text(self.pet["location"]!)
                            .font(.system(.body, design: .rounded))
                            .foregroundColor(.secondary)
                    }
                    HStack {
                        Text("Status")
                            .font(.system(.body, design: .rounded))
                            .foregroundColor(.secondary)
                        Spacer()
                        Text(self.pet["status"]!)
                            .font(.system(.body, design: .rounded))
                            .foregroundColor(self.statusColor(self.pet["status"]))
                    }
                }
                .padding()
                .background(Color("Secondary"))
                .cornerRadius(16)
            }
            .padding(.horizontal)
            .offset(y: 40)
        }
        .padding(.bottom, 40)
    }
}
