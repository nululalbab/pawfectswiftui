//
//  AnimalAlert.swift
//  ImpawsibbleSwiftUI
//
//  Created by Najibullah Ulul Albab on 11/08/20.
//  Copyright © 2020 Najibullah Ulul Albab. All rights reserved.
//

import SwiftUI

struct AnimalAlert: View {
    @State var show = false
    
    var body: some View {
        
        ZStack{
            
            NavigationView{
                
                Text("Add Something")
                    .navigationBarTitle("Home",displayMode: .inline)
                    .navigationBarItems(
                        
                        leading:
                        
                        Image("Nugget")
                            .resizable()
                            .frame(width: 30, height: 30)
                            .clipShape(Circle())
                        
                        , trailing:
                        
                        Button(action: {
                            
                            withAnimation{
                                
                                self.show.toggle()
                            }
                            
                        }, label: {
                            
                            Image("Julie")
                                .renderingMode(.original)
                                .resizable()
                                .frame(width: 15, height: 15)
                        })
                )
            }
            
            if self.show{
                
                GeometryReader{_ in
                    
                    AlertMenu()
                    
                }.background(
                    
                    Color.black.opacity(0.65)
                        .edgesIgnoringSafeArea(.all)
                        .onTapGesture {
                            
                            withAnimation{
                                
                                self.show.toggle()
                            }
                    }
                    
                )
            }
            
        }
    }
    
}

struct AnimalAlert_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView{
            AnimalAlert()
        }
        
    }
}

struct AlertMenu: View {
    
    
    var body: some View {
        VStack{
            Image("Adopter Icon Color")
                .renderingMode(.original)
                .resizable()
                .frame(width: 200, height: 200)
            
            Text("Oops")
                .font(.system(size: 22, weight: .bold  ,design: .rounded))
                .foregroundColor(Color("Primary Text"))
                .padding()
            Text("Kamu belum isi profil. Yuk isi profil dan ikuti kuis untuk hasil yang lebih akurat!")
                .padding()
                .font(.system(size: 14, weight: .regular  ,design: .rounded))
                .foregroundColor(Color("Primary Text"))
            NavigationLink(destination: PersonalInfoView()){
                ZStack{
                    
                    Rectangle()
                        .fill(Color.orange)
                        .padding(10)
                        .background(Color.orange)
                        .frame(height: 100)
                    Text("Ok, Siap")
                        .cornerRadius(10)
                }
                
                
            }
            .buttonStyle(PlainButtonStyle())
            
        }
        .background(Color("Secondary"))
        .cornerRadius(20)
        .padding()
    }
}

