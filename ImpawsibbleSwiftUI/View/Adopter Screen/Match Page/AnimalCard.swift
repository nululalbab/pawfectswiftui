//
//  AnimalCard.swift
//  ImpawsibbleSwiftUI
//
//  Created by Najibullah Ulul Albab on 29/07/20.
//  Copyright © 2020 Najibullah Ulul Albab. All rights reserved.
//

import SwiftUI

struct AnimalCard: View {
   var animal: [String: String]
    
    var body: some View {
        
            ZStack (alignment: Alignment.bottom) {
                Image(self.animal["imageName"] ?? "Nugget")
                    .resizable()
                    .aspectRatio(16/9, contentMode: .fit)
                    .cornerRadius(16)
                VStack {
                    VStack(alignment: HorizontalAlignment.leading) {
                        Text(self.animal["name"] ?? "Placeholder")
                            .font(.system(size: 16, weight: .bold, design: .rounded))
                            .foregroundColor(Color("Primary Text"))
                        HStack {
                            Text(self.animal["type"] ?? "Placeholder")
                                .font(.system(.body, design: .rounded))
                                .foregroundColor(.secondary)
                            Spacer()
                            Text(self.animal["location"] ?? "Placeholder")
                                .font(.system(.body, design: .rounded))
                                .foregroundColor(.secondary)
                        }
                    }
                    .padding()
                    .background(Color("Secondary"))
                    .cornerRadius(16)
                }
                .padding(.horizontal)
                .offset(y: 20)
            }
            .padding(.bottom, 20)
      
    }
}

struct AnimalCard_Previews: PreviewProvider {
    static var previews: some View {
        AnimalCard(animal: ["name": "Nugget", "type": "Border Collie", "location": "Surabaya", "imageName": "Nugget"])
    }
}
