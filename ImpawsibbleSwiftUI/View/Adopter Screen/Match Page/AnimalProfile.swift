//
//  AnimalProfile.swift
//  ImpawsibbleSwiftUI
//
//  Created by Najibullah Ulul Albab on 02/08/20.
//  Copyright © 2020 Najibullah Ulul Albab. All rights reserved.
//

import SwiftUI

struct AnimalProfile: View {
    @State var wishlist : Bool = false
    let pet: [String:String]
    var reason: String = "Nugget dibesarkan di rumah yang penuh kasih, sampai suatu hari pemiliknya meninggal dalam kecelakaan mobil tragis, memaksanya ditinggal di Shelter. Dia adalah anjing yang manis dan pintar, hubungi kami untuk info lebih banyak."
    var status: [Bool] = [true, true, false, false]
    
    var body: some View {
        ScrollView {
            VStack(alignment: .leading, spacing: 0) {
                ZStack(alignment: Alignment.bottom) {
                    Image(self.pet["imageName"]!)
                        .resizable()
                        .aspectRatio(4/3, contentMode: .fill)
                        .frame(height: 300, alignment: Alignment.top)
                        .offset(y:-50)
                        .clipped()
                    
                    Rectangle()
                        .frame(height: 32)
                        .cornerRadius(40)
                        .offset(y: -16)
                        .foregroundColor(.white)
                }
                
                VStack(alignment: .leading, spacing: 16){
                    ZStack{
                        Rectangle()
                            .fill(Color("Secondary"))
                            .cornerRadius(20)
                            .frame(width: 375, height: 120, alignment: .center)
                        VStack(alignment: .leading, spacing: 8){
                            Text(self.pet["name"]!)
                                .font(.system(.title, design: .rounded))
                                .bold()
                            HStack{
                                Text(self.pet["type"]!)
                                    .font(.system(.body, design: .rounded))
                                Spacer()
                                Text(self.pet["location"]!)
                                    .font(.system(.subheadline, design: .rounded))
                                    .frame(alignment: .bottomTrailing)
                            }
                        }
                        .padding()


                    }.offset(y: -20)

                    HStack(alignment: .center,spacing: 15){
                        ZStack{
                            Rectangle()
                                .fill(Color("Secondary"))
                                .frame(width: 80, height: 90, alignment: .leading)
                                .cornerRadius(20)
                            VStack(alignment: .center, spacing: 5){
                                Text("Umur")
                                    .font(.system(.body, design: .rounded))
                                    .bold()
                                Text("1 Year")
                                    .font(.system(.body, design: .rounded))
                            }
                        }
                        ZStack{
                            Rectangle()
                                .fill(Color("Secondary"))
                                .frame(width: 80, height: 90, alignment: .leading)
                                .cornerRadius(20)
                            VStack(alignment: .center, spacing: 5){
                                Text("Berat")
                                    .font(.system(.body, design: .rounded))
                                    .bold()
                                Text("1 Kg")
                                    .font(.system(.body, design: .rounded))
                            }
                        }
                        ZStack{
                            Rectangle()
                                .fill(Color("Secondary"))
                                .frame(width: 80, height: 90, alignment: .leading)
                                .cornerRadius(20)
                            VStack(alignment: .center, spacing: 5){
                                Text("Warna")
                                    .font(.system(.body, design: .rounded))
                                    .bold()
                                Text("Brown")
                                    .font(.system(.body, design: .rounded))
                            }
                        }
                        ZStack{
                            Rectangle()
                                .fill(Color("Secondary"))
                                .frame(width: 80, height: 90, alignment: .leading)
                                .cornerRadius(20)
                            VStack(alignment: .center, spacing: 5){
                                Text("Sex")
                                    .font(.system(.body, design: .rounded))
                                    .bold()
                                Text("♀")
                                    .font(.system(.body, design: .rounded))
                            }
                        }
                    }
                    .offset(y: -20)

                }
                .padding(.horizontal, 16)
                .padding(.bottom, 16)
            }
            Divider()
            VStack{
                VStack(alignment: .leading, spacing: 8){
                    Text("Latar Belakang")
                        .font(.system(size: 16, weight: .bold, design: .rounded))
                    Text(self.reason)
                }
                .padding()
                Divider()
                HStack(){
                    Text("Pemeliharaan")
                    Spacer()
                    Text("Sedang")
                }
                .padding(.horizontal)
                Divider()
                HStack(alignment: .top){
                    Text("Kepribadian")
                    Spacer()
                    VStack{
                        HStack(spacing: 10){
                            PersonalityCard(imageName: "ramah", text: "Ramah", status: true)
                            PersonalityCard(imageName: "loving", text: "Penyayang", status: false)
                            PersonalityCard(imageName: "adventurous", text: "Jiwa Bebas ", status: true)
                        }
                        HStack(spacing: 10){
                            PersonalityCard(imageName: "aloof", text: "Penyendiri", status: false)
                            PersonalityCard(imageName: "smart", text: "Pintar", status: false)
                            PersonalityCard(imageName: "shy", text: "Pemalu", status: false)
                        }
                    }
                }
                .padding(.horizontal)
                Divider()
                HStack(alignment: .top){
                    Text("Tingkah Laku")
                    Spacer()
                    VStack{
                        HStack(spacing: 10){
                            PersonalityCard(imageName: "playful", text: "Suka Main", status: true)
                            PersonalityCard(imageName: "loud", text: "Cerewet", status: true)
                            PersonalityCard(imageName: "lazy", text: "Malas", status: false)
                        }
                        HStack(spacing: 10){
                            PersonalityCard(imageName: "teritorial", text: "Teritorial", status: false)
                            PersonalityCard(imageName: "naughty", text: "Nakal", status: true)
                            PersonalityCard(imageName: "active", text: "Aktif", status: true)
                        }
                    }
                }
                .padding(.horizontal)
                Divider()
                VStack(alignment: .leading){
                    Text("Rescuer / Shelter")
                    HStack{
                        Text("Surabaya Animal Care Community")
                            .foregroundColor(Color.orange)
                        Spacer()
                        Image(systemName: "chevron.right")
                            .font(Font.system(size: 20, weight: .bold, design: .rounded))
                            .foregroundColor(Color.orange)
                    }
                }
                .padding()
                
            }
            HStack{
                ZStack{
                    
                    Button(action: {
                        self.wishlist.toggle()
                    }){
                        Image( systemName: self.wishlist ? "heart.fill" : "heart")
                            .foregroundColor(.red)
                            .padding()
                            .background(Color.orange)
                    }
                    .cornerRadius(10)
                    .padding(.leading)
                    
                    
                    
                    
                }
                ZStack{
                    Rectangle()
                        .fill(Color.orange)
                        .frame(width: .none, height: 50, alignment: .trailing)
                        .cornerRadius(10)
                        .padding(.trailing)
                    NavigationLink(destination: ExtraCare()){
                        
                        Text("Daftar Adopsi")
                    }
                    .foregroundColor(.white)
                    
                }
            }
        }
            
        .navigationBarTitle(
            Text("Animal Profile")
                .foregroundColor(.red)
                .font(.system(.largeTitle, design: .rounded))
        )
        
        
    }
}

struct AnimalProfile_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            AnimalProfile(pet: ["name": "Nugget", "type": "Border Collie", "location": "Surabaya", "imageName": "Nugget", "status": "Waiting List"])
        }
    }
}

extension View {
    public func addBorder<S>(_ content: S, width: CGFloat = 1, cornerRadius: CGFloat) -> some View where S : ShapeStyle {
        return overlay(RoundedRectangle(cornerRadius: cornerRadius).strokeBorder(content, lineWidth: width))
    }
}




struct PersonalityCard: View {
    var imageName: String
    var text: String
    var status: Bool
    var body: some View {
        VStack{
            Image(self.imageName)
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: 75, height: 75)
                .cornerRadius(10)
                .addBorder(self.status ? Color.orange : Color.gray, width: 1, cornerRadius: 10)
                .background(self.status ? Color("Secondary") : Color.white)
            Text(self.text)
                .font(.system(size: 12, weight: .regular, design: .rounded))
                .fixedSize(horizontal: true, vertical: true)
        }
    }
}
