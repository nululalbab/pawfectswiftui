//
//  SearchBarView.swift
//  LearnTMDBSwiftUI
//
//  Created by Najibullah Ulul Albab on 28/06/20.
//  Copyright © 2020 Najibullah Ulul Albab. All rights reserved.
//

import SwiftUI

struct SearchBar: UIViewRepresentable{
    func makeCoordinator() -> Coordinator {
        Coordinator(text: self.$text)
    }
    
   
    
    let placeholder: String
    @Binding var text: String
    
   func updateUIView(_ uiView:  UISearchBar, context: Context) {
    uiView.text = text
      }
    
    func makeUIView(context: Context) ->  UISearchBar {
        let searchBar = UISearchBar(frame: .zero)
        searchBar.placeholder = placeholder
        searchBar.searchBarStyle = .minimal
        searchBar.enablesReturnKeyAutomatically = false
        searchBar.delegate = context.coordinator
        return searchBar
    }
    
    
    class Coordinator: NSObject, UISearchBarDelegate {
        @Binding var text: String
        init(text: Binding<String>){
            _text = text
        }
        
        func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
            self.text = searchText
        }
        
        func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
            searchBar.resignFirstResponder()
        }
    }
}
