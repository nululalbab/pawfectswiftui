//
//  AnimalCarousel.swift
//  ImpawsibbleSwiftUI
//
//  Created by Najibullah Ulul Albab on 29/07/20.
//  Copyright © 2020 Najibullah Ulul Albab. All rights reserved.
//

import SwiftUI

struct AnimalCarousel: View {
    @EnvironmentObject var setting: UserSettings
    
    @State var show = false
    let animals: [[String:String]] = [["name": "Becky", "type":"Corgi", "location":"Surabaya", "imageName": "Becky"], ["name": "Julie", "type":"Retriever Mix", "location":"Surabaya", "imageName": "Julie"], ["name": "Nugget", "type":"Border Collie", "location":"Surabaya", "imageName": "Nugget"]]
    let newAnimals: [[String:String]] = [["name": "Sweetie", "type":"Anggora Mix", "location":"Surabaya", "imageName": "Sweetie"], ["name": "Cookie", "type":"Mix", "location":"Surabaya", "imageName": "Cookie"], ["name": "Nugget", "type":"Border Collie", "location":"Surabaya", "imageName": "Nugget"], ["name": "Sukti", "type":"Tabby", "location":"Surabaya", "imageName": "Sukti"]]
    @State var clicked: [Bool] = [true, false, false, false, false, false, false, false]
    @State var searchBarText: String = ""
    let filter: [String] = ["All","Kucing", "Anjing", "Jantan", "Betina","Pintar", "Aktif","Ramah"]
    var body: some View {
        ScrollView{
            ZStack(alignment: .center){
                VStack{
                    SearchBar(placeholder: "Search animals..", text: $searchBarText )
                    ScrollView(.horizontal, showsIndicators: false){
                        HStack{
                            ForEach(0..<self.filter.count, id: \.self){ index in
                                FilterPet(title: self.filter[index], clicked: self.$clicked[index])
                            }
                        }
                    }
                    .padding(10)
                    ForEach(self.setting.userAssessment ? newAnimals : animals , id: \.self){ result in
                        Group{
                            if(self.setting.userAssessment) {
                                NavigationLink(destination: AnimalProfile(pet: result)){
                                    AnimalCard(animal: result)
                                }
                                .buttonStyle(PlainButtonStyle())
                            }else {
                                Button(action: {
                                    self.show.toggle()
                                }, label: {
                                    AnimalCard(animal: result)
                                })
                                .buttonStyle(PlainButtonStyle())
                                                            
                            }
                        }
                    }
                    .padding(.horizontal,30)
                    if(self.setting.userAssessment == false){
                        NavigationLink(destination: PersonalInfoView()){
                            Text("Tambahkan profil untuk hasil lebih akurat")
                                .frame(minWidth: 0, maxWidth: .infinity)
                                .padding()
                                .background(Color.orange)
                                .foregroundColor(.white)
                                .cornerRadius(16)
                        }
                        .padding(.bottom, 40)
                        .padding(.horizontal,30)
                    }
                    
                }
                
                .navigationBarTitle(Text("Adopsi Sahabatmu"))
                
                if self.show{
                    
                    GeometryReader{_ in
                        
                        AlertMenuOops()
                        
                    }.background(
                        
                        Color.black.opacity(0.65)
                            .edgesIgnoringSafeArea(.all)
                            .onTapGesture {
                                
                                withAnimation{
                                    
                                    self.show.toggle()
                                }
                        }
                        
                    )
                }
            }
            
        }
        
        
        
    }
}

struct AnimalCarousel_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            AnimalCarousel()
        }
    }
}

struct FilterPet: View {
    var title: String
    @Binding var clicked: Bool
    
    var body: some View {
        Button(action: {
            self.clicked.toggle()
        }) {
            Text(self.title)
                .padding(10)
                .background(self.clicked ? Color.orange : Color.white)
                .overlay(
                    RoundedRectangle(cornerRadius: 10)
                        .stroke(self.clicked ? Color.white : Color.black, lineWidth: 1)
            )
                .cornerRadius(10)
        }
        .buttonStyle(PlainButtonStyle())
    }
}

struct AlertMenuOops: View {
    
    
    var body: some View {
        VStack{
            Image("fail")
                .renderingMode(.original)
                .resizable()
            .scaledToFill()
                .frame(width: 200, height: 200)
            
            Text("Oops")
                .font(.system(size: 22, weight: .bold  ,design: .rounded))
                .foregroundColor(Color("Primary Text"))
                .padding()
            Text("Kamu belum isi profil. Yuk isi profil dan ikuti kuis untuk hasil yang lebih akurat!")
                .padding()
                .font(.system(size: 14, weight: .regular  ,design: .rounded))
                .foregroundColor(Color("Primary Text"))
            NavigationLink(destination: PersonalInfoView()){
                ZStack{
                    
                    Rectangle()
                        .fill(Color.orange)
                        .padding(10)
                        .background(Color.orange)
                        .frame(height: 50)
                    Text("Ok, Siap")
                        .cornerRadius(10)
                }
                
                
            }
            .buttonStyle(PlainButtonStyle())
            
        }
        .background(Color("Secondary"))
        .cornerRadius(20)
        .padding()
    }
}
