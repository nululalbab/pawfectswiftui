//
//  AggreementPage.swift
//  ImpawsibbleSwiftUI
//
//  Created by Najibullah Ulul Albab on 04/08/20.
//  Copyright © 2020 Najibullah Ulul Albab. All rights reserved.
//

import SwiftUI

struct AgreementPage: View {
    @State var checked: [Bool] = [false, false, false]
    
     var agreements: [String] = ["Apakah anda bersedia mensterilkan hewan peliharaan Anda?", "Apakah Anda bersedia memberikan vaksin untuk hewan peliharaan Anda?", "Saat hewan peliharaan sakit dan butuh banyak biaya perawatan, apakah Anda bersedia memberi perawatan untuk hewan tersebut (lewat prosedur medis)?"]
    
    var descriptions: String = """
    SACC adalah organisasi/ komunitas bersifat nirlaba (non-profit) sehingga diharapkan Calon Adopter bersedia untuk turut meringankan biaya vaksinasi dan sterilisasi yang telah dilakukan terhadap calon hewan peliharaan dengan memberikan sumbangan minimal sebesar Rp.150.000,- , yang mana donasi tersebut akan digunakan untuk memberikan perawatan bagi hewan-hewan terlantar selanjutnya.
    
    Dengan ini saya menyetujui segala syarat dan ketentuan untuk mengadopsi hewan secara sukarela tanpa ada paksaan dari pihak manapun dan saya dengan sadar sepenuhnya akan bertanggung tanggung jawab atas hewan yang akan saya adopsi hingga akhir masa hidupnya.
    """
    
    var body: some View {
        ScrollView{
            ZStack(alignment: .topLeading){
                VStack(alignment: .leading, spacing: 8){
                    Text("* Wajib Diisi")
                        .foregroundColor(.red)
                    
                    ForEach(0..<agreements.count){ index in
                        CheckboxAgreement(checked: self.$checked[index], agreement: self.agreements[index])
                        
                    }
                    
                    Text(self.descriptions)
                        .padding(.vertical, 20)
                        
                    NavigationLink(destination: GreetingCard()){
                        Text("Saya Setuju")
                            .frame(minWidth: 0, maxWidth: .infinity)
                            .padding()
                            .background(Color.orange)
                            .foregroundColor(.white)
                            .cornerRadius(16)
                    }
                }
            .padding()
            }
        .navigationBarTitle(Text("Perjanjian Shelter"))
        .padding()
        }
    }
}

struct AggreementPage_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView{
            AgreementPage()
        }
        
    }
}

struct CheckboxAgreement: View {
    @Binding var checked: Bool
    var agreement: String
    var body: some View {
        Button(action: {
            self.checked.toggle()
        }) {
            HStack{
                Image(systemName: self.checked ? "checkmark.square.fill" : "square")
                    .foregroundColor(self.checked ? .orange : .gray)
                    .frame(width: 40, height: 40, alignment: .center)
                Spacer()
                Text(self.agreement)
                    .font(.system(size: 16, weight: .regular, design: .rounded))
                    .foregroundColor(Color("Primary Text"))
            }
        }
    }
}
