//
//  ExtraCare.swift
//  ImpawsibbleSwiftUI
//
//  Created by Najibullah Ulul Albab on 04/08/20.
//  Copyright © 2020 Najibullah Ulul Albab. All rights reserved.
//

import SwiftUI

struct ExtraCare: View {
    var extracares: [[String:String]] = [["extracare" : "Vaksinasi Tahunan"], ["extracare" : "Hanya makan wet food kaleng"]]
    
    var body: some View {
        VStack {
            VStack(alignment: .leading, spacing: 0) {
                ZStack(alignment: Alignment.bottom) {
                    Image("Nugget")
                        .resizable()
                        .aspectRatio(4/3, contentMode: .fill)
                        .frame(height: 300, alignment: Alignment.top)
                        .offset(y:-50)
                        .clipped()

                    Rectangle()
                        .frame(height: 32)
                        .cornerRadius(40)
                        .offset(y: -16)
                        .foregroundColor(.white)
                }
            }
            
            VStack{
                Text("Nugget membutuhkan beberapa perhatian ekstra. Dia butuh: ")
                    .font(.system(size: 22, weight: .bold  ,design: .rounded))
                    .foregroundColor(Color("Primary Text"))
                    .padding(.horizontal,8)
                    .padding(.vertical,8)
                    .offset(y:-30)
                VStack(alignment: .leading, spacing: 8){
                    ForEach(0..<extracares.count){ index in
                        
                        Text(self.extracares[index]["extracare"]!)
                        .font(.system(size: 16, weight: .regular, design: .rounded))
                        .foregroundColor(Color("Primary Text"))
                    }
                }
                Spacer()
                
                Text("Apakah Anda bersedia memenuhi kebutuhan khusus Nugget?")
                .font(.system(size: 22, weight: .bold  ,design: .rounded))
                .foregroundColor(Color("Primary Text"))
                .padding(.horizontal,8)
                .padding(.vertical,8)
                
                NavigationLink(destination: AgreementPage()){
                    Text("Daftar Adopsi")
                        .frame(minWidth: 0, maxWidth: .infinity)
                        .padding()
                        .background(Color.orange)
                        .foregroundColor(.white)
                        .cornerRadius(16)
                }
                .padding()
            }
        }
        .navigationBarTitle(
            Text("Extra Care")
                .foregroundColor(.red)
                .font(.system(.largeTitle, design: .rounded))
        )
    }
    
}

struct ExtraCare_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView{
           ExtraCare()
        }
        
    }
}
