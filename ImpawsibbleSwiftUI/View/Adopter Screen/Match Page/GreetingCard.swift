//
//  GreetingCard.swift
//  ImpawsibbleSwiftUI
//
//  Created by Najibullah Ulul Albab on 04/08/20.
//  Copyright © 2020 Najibullah Ulul Albab. All rights reserved.
//

import SwiftUI

struct GreetingCard: View {
    var body: some View {
        ScrollView{
            VStack {
                VStack(alignment: .leading, spacing: 0) {
                    ZStack(alignment: Alignment.bottom) {
                        Image("Nugget")
                            .resizable()
                            .aspectRatio(4/3, contentMode: .fill)
                            .frame(height: 300, alignment: Alignment.top)
                            .offset(y:-50)
                            .clipped()

                        Rectangle()
                            .frame(height: 32)
                            .cornerRadius(40)
                            .offset(y: -16)
                            .foregroundColor(.white)
                    }
                }
                
                VStack(alignment: .leading){
                    Text("Terima kasih telah mendaftar! Shelter akan segera menghubungi anda!")
                        .font(.system(size: 22, weight: .bold  ,design: .rounded))
                        .foregroundColor(Color("Primary Text"))
                        .padding(.horizontal,8)
                        .padding(.vertical,8)
                        .offset(y:-30)
                    Text("Disclaimer")
                        .foregroundColor(.red)
                        .padding(8)
                    Text("Dengan mendaftar profil Anda akan ditinjau oleh shelter terlebih dahulu, kemudian shelter akan menghubungi Anda nanti. Sebelum Shelter/Rescuer menerima pendaftaran Anda dan proses penyaringan Anda diklaim berhasil, hewan peliharaan tersebut tetap bukan milik Anda. Harap tunggu kabar dan tindak lanjut dari Shelter.")
                        .padding(8)
                    Spacer()
                    
                    NavigationLink(destination: AgreementPage()){
                        Text("Kembali ke Halaman Utama")
                            .frame(minWidth: 0, maxWidth: .infinity)
                            .padding()
                            .background(Color.orange)
                            .foregroundColor(.white)
                            .cornerRadius(16)
                    }
                    .padding()
                }
            .padding()
            }
        }
        
        .navigationBarTitle(
            Text("")
                .foregroundColor(.red)
                .font(.system(.largeTitle, design: .rounded))
        )
    }
}

struct GreetingCard_Previews: PreviewProvider {
    static var previews: some View {
        GreetingCard()
    }
}
