//
//  AdopterTabView.swift
//  ImpawsibbleSwiftUI
//
//  Created by Michael Randicha Gunawan Santoso on 31/07/20.
//  Copyright © 2020 Najibullah Ulul Albab. All rights reserved.
//

import SwiftUI

struct AdopterTabView: View {
    @EnvironmentObject var settings: UserSettings
    
    var body: some View {
        TabView {
            NavigationView {
                AnimalCarousel()
                    .navigationBarItems(trailing:
                        
                            NavigationLink(destination: WishlistView())
                            {
                                Image(systemName: "heart.fill")
                                    .foregroundColor(.orange)
                            }
                            .buttonStyle(PlainButtonStyle())
                  )
            }
            .tabItem {
                Image(systemName: "hare.fill")
                Text("Adopt")
            }
            
            NavigationView {
                AdopterProfile()
            }
            .tabItem {
                Image(systemName: "person.fill")
                Text("Profile")
            }
        }
        .accentColor(.orange)
    }
}

struct AdopterTabView_Previews: PreviewProvider {
    static var previews: some View {
        AdopterTabView()
    }
}
