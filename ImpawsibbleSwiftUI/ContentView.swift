//
//  ContentView.swift
//  ImpawsibbleSwiftUI
//
//  Created by Najibullah Ulul Albab on 27/07/20.
//  Copyright © 2020 Najibullah Ulul Albab. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @EnvironmentObject var settings: UserSettings
    
    var body: some View {
        ZStack {
            if !settings.onBoardingFinished {
                OnboardingView()
            } else if settings.userType == "" {
                UserSelectionView()
                    .transition(.move(edge: .trailing))
            } else if settings.userType == "Rescuer" {
                RescuerTabView()
                    .transition(.move(edge: .trailing))
            } else if settings.userType == "Adopter" {
                AdopterTabView()
                    .transition(.move(edge: .trailing))
            } else {
                EmptyView()
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    
    static var previews: some View {
        ContentView().environmentObject(UserSettings())
    }
}
