//
//  ContentView.swift
//  ImpawsibbleSwiftUI
//
//  Created by Najibullah Ulul Albab on 27/07/20.
//  Copyright © 2020 Najibullah Ulul Albab. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @EnvironmentObject var settings: UserSettings
    
    var body: some View {
        if !settings.onBoardingFinished {
            return AnyView(OnboardingView())
        } else if settings.userType == "" {
            return AnyView(UserSelectionView())
        } else if settings.userType == "Rescuer" {
            return AnyView(RescuerTabView())
        } else if settings.userType == "Adopter" {
            return AnyView(AdopterTabView())
        } else {
            return AnyView(EmptyView())
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    
    static var previews: some View {
        ContentView().environmentObject(UserSettings())
    }
}
