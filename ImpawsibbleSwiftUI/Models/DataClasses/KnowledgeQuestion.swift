//
//  KnowledgeQuestion.swift
//  ImpawsibbleSwiftUI
//
//  Created by Najibullah Ulul Albab on 12/08/20.
//  Copyright © 2020 Najibullah Ulul Albab. All rights reserved.
//

import Foundation


struct KnowledgeQuestion: Decodable, Identifiable, Equatable {
    let id: UUID = UUID()
    let question: String
    var answer: String
    let options: [String]
    static func == (lhs: KnowledgeQuestion, rhs: KnowledgeQuestion) -> Bool {
        lhs.id == rhs.id && lhs.answer == rhs.answer && lhs.question == rhs.question
    }
}

extension KnowledgeQuestion {
    static func getQuestions() -> [KnowledgeQuestion] {
        return [
            KnowledgeQuestion(question: "Dimana anda tinggal?", answer: "", options: [
                "Rumah", "Apartemen", "Lainnya"
            ]),
            KnowledgeQuestion(question: "Apakah tempat tinggal anda memiliki halaman?", answer: "", options: [
                "Ya", "Tidak"
            ]),
            KnowledgeQuestion(question: "Dengan siapa anda tinggal?", answer: "", options: [
                "Sendirian", "Orang Lain"
            ]),
            KnowledgeQuestion(question: "Apakah anda pernah memiliki hewan peliharaan?", answer: "", options: []),
            KnowledgeQuestion(question: "Apakah anda punya hewan peliharaan?", answer: "", options: []),
            KnowledgeQuestion(question: "Tahukah Anda tentang vaksinasi hewan peliharaan berdasarkan usia?", answer: "", options: []),
            KnowledgeQuestion(question: "Apakah Anda tahu tentang penyakit hewan peliharaan?", answer: "", options: []),
            KnowledgeQuestion(question: "Siapa saja yang akan merawat hewan tersebut?", answer: "", options: [
                "Saya Sendiri", "Keluarga", "Lainnya"
            ]),
            KnowledgeQuestion(question: "Berapa waktu dalam sehari yang dapat anda sisihkan untuk merawat dan bermain dengan hewan peliharaan?", answer: "", options: []),
            KnowledgeQuestion(question: "Apa yang akan anda lakukan bila anda perlu meninggalkan hewan peliharaan selama beberapa hari?", answer: "", options: []),
            KnowledgeQuestion(question: "Apakah semua anggota keluarga/orang yang tinggal dengan Anda setuju Anda mengadopsi hewan peliharaan?", answer: "", options: [
                "Ya", "Tidak"
            ]),
            KnowledgeQuestion(question: "Apakah anda bersedia menjaga dan memelihara hewan tersebut hingga akhir hayat mereka?", answer: "", options: [
                "Ya", "Tidak"
            ])
        ]
    }
}
