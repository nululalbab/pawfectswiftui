//
//  OnboardingData.swift
//  ImpawsibbleSwiftUI
//
//  Created by Michael Randicha Gunawan Santoso on 12/08/20.
//  Copyright © 2020 Najibullah Ulul Albab. All rights reserved.
//

import Foundation

struct OnboardingData: Decodable, Identifiable, Equatable {
    let id: UUID = UUID()
    let title: String
    let subtitle: String
    
    static func == (lhs: OnboardingData, rhs: OnboardingData) -> Bool {
        return lhs.id == rhs.id
    }
}

extension OnboardingData {
    static func getData() -> [OnboardingData] {
        return [
            OnboardingData(
                title: """
                    Halo!
                    Selamat datang
                    di Pawfect!
                    """,
                subtitle: "Temukan sahabat terbaikmu."),
            OnboardingData(
                title: """
                    Berikan rumah
                    untuk hewan
                    terlantar!
                    """,
                subtitle: "Adopsi jangan membeli."),
            OnboardingData(
                title: """
                    Adopsi kini
                    lebih mudah!
                    """,
                subtitle: "Proses adopsi yang lebih efisien.."),
        ]
    }
}
